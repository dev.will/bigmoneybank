const { Router } = require("express");
const router = Router();

const {
    getCuenta, 
    getCuentaDestino, 
} = require("../controllers/cuenta.controller");

router.get("/cuenta/:id", getCuenta);
router.get("/cuentaDestino/:id", getCuentaDestino);
module.exports = router;