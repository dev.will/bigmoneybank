const database = require("../config/database");
const { errorHandler } = require("./../utils/errorHandler");

const spCreateUsuario = async (req, res) => {
  const { usuario, contrasena, tipo_cuenta } = req.body;
  try {
    const response = await database.run(
      "CALL SP_CREAR_USUARIO(:1, :2, :3)",
      [usuario, contrasena, tipo_cuenta],
      { autoCommit: true }
    );
    if (response) {
      res.json({
        message: "Guardado con éxito",
        estado: true,
      });
    }
  } catch (error) {
    res.json({
      message: "Creación de usuario fallida, contacte con el administrador.",
      estado: false,
    });
  }
};

const funIniciarSesion = async (req, res) => {
  const { usuario, contrasena } = req.body;
  const response = await database.run(
    "SELECT * FROM CUENTA JOIN USUARIO ON CUENTA.ID_USUARIO = USUARIO.ID_USUARIO WHERE USUARIO.USUARIO = " +
      usuario +
      " AND USUARIO.CONTRASENA = " +
      contrasena
  );
  if (response.rows && response.rows.length) {
    res.json({
      message: "Logueado con Exito!",
      estado: true,
      cuenta: response.rows[0],
    });
  } else {
    res.json({
      message: "No se logueo",
      estado: false,
    });
  }
};

const spActualizarContrasena = async (req, res) => {
  const { nombre_usuario, contrasena_actual, contrasena_nueva } = req.body;
  try {
    const response = await database.run(
      "CALL SP_ACTUALIZAR_CONTRASENA(:1, :2, :3)",
      [nombre_usuario, contrasena_actual, contrasena_nueva],
      { autoCommit: true }
    );
    if (response) {
      res.json({
        message: "Actualizado con éxito",
        estado: true,
      });
    }
  } catch (error) {
    res.json({
      message: errorHandler(error),
      estado: false,
    });
  }
};

module.exports = {
  spCreateUsuario,
  funIniciarSesion,
  spActualizarContrasena,
};
