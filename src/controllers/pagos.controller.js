const database = require("../config/database");
const { errorHandler } = require("./../utils/errorHandler");

const getHistorialIntereses = async(req, res) => {
  const numero_cuenta = parseInt(req.params.id);
  try {
      const response = await database.run(
          `SELECT * FROM HISTORIAL_INTERESES WHERE NUMERO_CUENTA = :1`, [numero_cuenta]
      );
      if (response) {
          res.json({
              message: "Datos obtenidos!",
              estado: true,
              historial: response.rows,
          });
      }
  } catch (error) {
      res.json({
          message: errorHandler(error),
          estado: false,
      });
  }
};

module.exports = { getHistorialIntereses };