const database = require("../config/database");
const { errorHandler } = require("./../utils/errorHandler");

const getHistorialTransacciones = async(req, res) => {
    const numero_cuenta = parseInt(req.params.id);
    try {
        const response = await database.run(
            `SELECT * FROM HISTORIAL_TRANSACCIONES WHERE NUMERO_CUENTA = :1`, [numero_cuenta]
        );
        if (response) {
            res.json({
                message: "Datos obtenidos!",
                estado: true,
                historial: response.rows,
            });
        }
    } catch (error) {
        res.json({
            message: errorHandler(error),
            estado: false,
        });
    }
  };

const spTransaccion = async (req, res) => {
  const { CUENTA_ORIGEN, CUENTA_DESTINO, MONTO, DESCRIPCION } = req.body;
  try {
    const response = await database.run(
      "CALL SP_transaccion(:1, :2, :3, :4)",
      [CUENTA_ORIGEN, CUENTA_DESTINO, MONTO, DESCRIPCION],
      { autoCommit: true }
    );
    if (response) {
      res.json({
        message: "Transaccion exitosa!",
        estado: true,
      });
    }
  } catch (error) {
    res.json({
      message: errorHandler(error),
      estado: false,
    });
  }
};

module.exports = {
  getHistorialTransacciones,
  spTransaccion,
};
