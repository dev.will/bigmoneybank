const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
    );
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.get("/", (req, res) => {
    res.send("Hello World!");
});

// Routes
app.use(require("./routes/usuario"));
app.use(require("./routes/transacciones"));
app.use(require("./routes/cuenta"));
app.use(require("./routes/pagos"));

app.listen(3000);
console.log("Server on port", port);