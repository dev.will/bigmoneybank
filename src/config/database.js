const oracledb = require("oracledb");
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

try {
  oracledb.initOracleClient({ libDir: "C:\\oracle\\instantclient_12_2" });
} catch (err) {
  console.error("Error init db process: ", error);
}

const credenciales = {
  USER: '"bank"',
  PASSWORD: "root",
  SERVER: "40.121.147.119",
  SID: "ORCLCDB",
  PORT: "1521",
};

const conectar = (credenciales) =>
  oracledb.getConnection({
    user: credenciales.USER,
    password: credenciales.PASSWORD,
    connectString:
      "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(Host=" +
      credenciales.SERVER +
      ")(Port=" +
      credenciales.PORT +
      "))(CONNECT_DATA=(SID=" +
      credenciales.SID +
      ")))",
  });

async function run(...args) {
  const conexion = await conectar(credenciales);
  const response = conexion.execute(...args);
  conexion.close();
  return response;
}

module.exports = { run };
