/**
 * Funcion que trata los errores de API, y devuelve el primer error obtenido.
 * @param {error} error devuelto del api
 * @returns string que identifica el primer error ocurrido
 */
function errorHandler(error) {
  const errorLines = error.message.split("\n");
  const errorMessage = errorLines[0].split(":")[1];
  return errorMessage.substring(1, errorMessage.length);
}
module.exports = { errorHandler };
