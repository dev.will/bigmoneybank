const { Router } = require("express");
const router = Router();

const {
    spCreateUsuario,
    funIniciarSesion,
    spActualizarContrasena,
} = require("../controllers/usuario.controller");

router.post("/registrar", spCreateUsuario);
router.post("/iniciarSesion", funIniciarSesion);
router.put("/actualizarContrasena", spActualizarContrasena);
module.exports = router;