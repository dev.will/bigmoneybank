const { Router } = require("express");
const router = Router();

const {
    getHistorialIntereses,
} = require("../controllers/pagos.controller");

router.get("/historialIntereses/:id", getHistorialIntereses);
module.exports = router;