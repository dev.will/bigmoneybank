const { Router } = require("express");
const router = Router();

const {
  getHistorialTransacciones,
  spTransaccion,
} = require("../controllers/transacciones.controller");

router.get("/historialTransaccion/:id", getHistorialTransacciones);
router.post("/transaccion", spTransaccion);
module.exports = router;
