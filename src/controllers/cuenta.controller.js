const database = require("../config/database");
const { errorHandler } = require("./../utils/errorHandler");

const getCuenta = async (req, res) => {
  const numero_cuenta = parseInt(req.params.id);
  try {
    const response = await database.run(
      `SELECT * FROM CUENTA WHERE NUMERO_CUENTA = :1`,
      [numero_cuenta]
    );
    if (response) {
      res.json({
        message: "Datos obtenidos!",
        estado: true,
        cuenta: response.rows[0],
      });
    }
  } catch (error) {
    res.json({
      message: errorHandler(error),
      estado: false,
    });
  }
};

const getCuentaDestino = async (req, res) => {
  const numero_cuenta = parseInt(req.params.id);
  try {
    const response = await database.run(
      `SELECT NOMBRE_CUENTA, NUMERO_CUENTA FROM CUENTA WHERE NUMERO_CUENTA = :1`,
      [numero_cuenta]
    );
    if (response) {
      res.json({
        message: "Datos obtenidos!",
        estado: true,
        cuenta: response.rows[0],
      });
    }
  } catch (error) {
    res.json({
      message: errorHandler(error),
      estado: false,
    });
  }
};

module.exports = { getCuenta, getCuentaDestino };
